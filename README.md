# limeClient

holds the smart contracts and the web-app running the user interface to interact with it

## for contract development

[install embark](https://embark.status.im/docs/installation.html)
Ethereum (geth-client) & IPFS are only optional

> npm i -g embark
