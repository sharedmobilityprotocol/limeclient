module.exports = {
"SimpleStorage": require('./SimpleStorage').default,
"ENSRegistry": require('./ENSRegistry').default,
"Resolver": require('./Resolver').default,
"FIFSRegistrar": require('./FIFSRegistrar').default,
};